#!/bin/bash

cd "$(dirname $(realpath "$0"))/.."

if [ -z "$TOR_BROWSER_BUILD" ]; then
	TOR_BROWSER_BUILD=../../tor-browser-build
fi

android_service="$(ls -1td "$TOR_BROWSER_BUILD/out/tor-android-service/"tor-android-service-* | head -1)"
if [ -z "$android_service" ]; then
	echo "Cannot find Tor Android Service artifacts!"
	exit 1
fi

onion_proxy_library="$(ls -1td "$TOR_BROWSER_BUILD/out/tor-onion-proxy-library/"tor-onion-proxy-library-* | head -1)"
if [ -z "$onion_proxy_library" ]; then
	echo "Cannot find Tor Onoin Proxy library artifacts!"
	exit 2
fi

cp "$android_service"/* app/
cp "$onion_proxy_library"/* app/

noscript="$(find "$TOR_BROWSER_BUILD/out/browser" -name 'noscript*.xpi' -print | sort | tail -1)"
mkdir -p "app/src/main/assets/extensions"
if [ -f "$noscript" ]; then
	cp "$noscript" "app/src/main/assets/extensions/{73a6fe31-595d-460b-a920-fcc0f8843232}.xpi"
fi
